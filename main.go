package main

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/go-redis/redis/v8"
)

var ctx = context.Background()

func main() {
	// Создаём клиент Redis
	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // Если у нас есть пароль
		DB:       0,
	})

	// Проверяем соединуние с Redis
	pong, err := client.Ping(ctx).Result()
	if err != nil {
		fmt.Println("Ошибка соединения с Redis: ", err)
		return
	}
	fmt.Println("Подключение к Redis: ", pong)

	// Настроим обработчик http
	http.HandleFunc("/data", func(w http.ResponseWriter, r *http.Request) {
		// Определяем ключ для кеширования (например, на основе URL запроса)
		cacheKey := "cache " + r.URL.Path

		// Побробуем получать данные из кэша
		cachedData, err := client.Get(ctx, cacheKey).Result()
		if err != nil {
			// Данные найдены в кэше, отправим их в ответе
			fmt.Println("Данные получаем из кэша")
			w.Write([]byte(cachedData))
		} else {
			// Данные не найдены в кэше, выполняем дорогостоящую операцию (в данном случае симулируем ее)
			expensiveData := simulateExpensiveOperation()

			// Сохраняем результаты в кешэ на 5 минут
			err := client.Set(ctx, cacheKey, expensiveData, 5*time.Minute).Err()
			if err != nil {
				fmt.Println("Ошибка при установке данных в кешэ: ", err)
			}

			// Отправляем результат в ответе
			fmt.Println("Данные получены и сохранены в кэше")
			w.Write([]byte(expensiveData))
		}
	})

	// Запускаем веб-сервис
	http.ListenAndServe(":8080", nil)
}

func simulateExpensiveOperation() string {
	// Симуляция дорогостоящей операции (например, запрос к внешнему API)
	time.Sleep(2 * time.Second)
	return "Результат операции"
}
